import string
import warnings

from openpyxl import Workbook
from openpyxl.styles.borders import Border, Side
from openpyxl.styles import Alignment
from openpyxl.styles import Font

THIN_BORDER = Border(
    left=Side(style="thin"),
    right=Side(style="thin"),
    top=Side(style="thin"),
    bottom=Side(style="thin"),
)

TITLE_HEIGHT = 40
NORMAL_HEIGHT = 25
BOLD = Font(name="Arial", bold=True)
CENTRE = Alignment(horizontal="center", vertical="center")
DEFAULT_WIDTH = 20


class ExcelWorkbook:
    def __init__(self, dt):
        self.workbook = Workbook()
        # remove any sheets in the newly created book to ensure a clean slate
        for sheet in self.workbook.worksheets:
            self.workbook.remove(sheet)
        self.dt = dt
        self.active_sheet = None

    def add_sheet(self, title, headers, widths={}):
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            # ignore warnings, to suppress message about overly-long titles
            self.active_sheet = self.workbook.create_sheet(title=title)
        self.active_sheet.page_setup.paperSize = self.active_sheet.PAPERSIZE_A4
        self.active_sheet.page_margins.top = 0.5
        self.active_sheet.page_margins.bottom = 0.2
        self.active_sheet.page_margins.left = 0.2
        self.active_sheet.page_margins.right = 0.2

        self.active_sheet["A1"] = f"{title} as at {self.dt:%H:%M on %Y/%m/%d}"
        row_num = 2
        for (n, value) in enumerate(headers):
            letter = string.ascii_uppercase[n]
            cell = f"{letter}{row_num}"
            self.active_sheet.column_dimensions[letter].width = widths.get(
                value, DEFAULT_WIDTH
            )
            self.active_sheet[cell] = value
            self.active_sheet[cell].font = BOLD
            self.active_sheet[cell].alignment = CENTRE
            self.active_sheet[cell].border = THIN_BORDER
        self.active_sheet.row_dimensions[1].height = TITLE_HEIGHT

    def add_row(self, values):
        row_num = self.active_sheet.max_row + 1
        for (col_num, value) in enumerate(values, 0):
            letter = string.ascii_uppercase[col_num]
            cell = f"{letter}{row_num}"
            self.active_sheet[cell].border = THIN_BORDER
            self.active_sheet[cell] = value
        self.active_sheet.row_dimensions[row_num].height = NORMAL_HEIGHT

    def save(self, filename):
        self.workbook.save(filename)


if __name__ == "__main__":
    from datetime import datetime

    wb = ExcelWorkbook(datetime.now())
    wb.add_sheet(
        "First But With Really Long Name!!", ["abc", "def", "ghi"], widths={"def": 100}
    )
    wb.add_row(["1", "2", "34567890"])
    wb.add_sheet("Second", ["ab", "cdef", "gh"], widths={"gh": 50})
    wb.save("test.xlsx")
