from collections import Counter, defaultdict
from datetime import datetime
import os

import clubs
import excel

from attrs import asdict, define
from dotenv import load_dotenv
from pymongo import MongoClient
from rich import print

load_dotenv()

client = MongoClient(
    host=os.getenv("MONGODB_HOST"),
    port=int(os.getenv("MONGODB_PORT")),
    username=os.getenv("MONGODB_USERNAME"),
    password=os.getenv("MONGODB_PASSWORD"),
)
db = client["md410_2022_conv"]
collection = db["reg_form"]

WINDBREAKER_KEYS = ["S", "M", "L", "XL", "2XL", "3XL", "4XL", "5XL"]


@define
class Attendee:
    reg_num: int
    name: str
    name_badge: str
    club: str
    district: str
    pins: int
    transport: int
    windbreakers: str
    dietary: str
    disability: str
    first_names: str
    last_name: str


@define
class Stats:
    total_attendees: int = 0
    lions: int = 0
    non_lions: int = 0
    first_mdc: int = 0
    mjf_lunch: int = 0
    pdg_dinner: int = 0
    lpe_breakfast: int = 0
    partner_program: int = 0
    pins: int = 0
    transport: int = 0
    windbreakers: dict = None
    clubs: Counter = None
    districts: Counter = None
    dietary: list = None
    disability: list = None
    attendees: list = None

    def populate(self, collection=None):
        records = list(collection.find())
        self.clubs = Counter()
        self.districts = Counter()
        self.dietary = []
        self.disability = []
        self.windbreakers = defaultdict(int)
        self.attendees = []
        for record in records:
            attendee = record["attendees"][0]
            if attendee["lion"]:
                self.lions += 1
            else:
                self.non_lions += 1
            for attr in (
                "first_mdc",
                "mjf_lunch",
                "pdg_dinner",
                "lpe_breakfast",
                "partner_program",
            ):
                if attendee[attr]:
                    setattr(self, attr, getattr(self, attr) + 1)
            self.clubs[attendee["club"]] += 1
            self.districts[clubs.CLUBS.get(attendee["club"])] += 1
            self.pins += int(record["items"]["pins"])
            self.transport += int(record["items"].get("transport", 0))
            for wb in record["items"]["windbreakers"]:
                self.windbreakers[wb] += 1
            locals = {}
            for attr in ("dietary", "disability"):
                if attendee[attr] and attendee[attr].lower() not in (
                    "none",
                    "no",
                    "non",
                    "n/a",
                    "na",
                    "nil",
                    "not applicable",
                ):
                    getattr(self, attr).append(attendee[attr])
                    locals[attr] = attendee[attr]
            self.attendees.append(
                Attendee(
                    record["reg_num"],
                    attendee["full_name"],
                    attendee["name_badge"],
                    attendee["club"],
                    clubs.CLUBS.get(attendee["club"]),
                    record["items"]["pins"],
                    record["items"].get("transport", 0),
                    ";".join(record["items"]["windbreakers"])
                    if record["items"]["windbreakers"]
                    else "N/A",
                    locals.get("dietary", "None"),
                    locals.get("disability", "None"),
                    attendee["first_names"],
                    attendee["last_name"],
                )
            )

        self.total_attendees = self.lions + self.non_lions
        for record in records:
            if record["attendees"][0]["club"] not in clubs.CLUBS:
                print(f"Unspecified District: {record}")

    def make_excel(self, file_name="md_convention_2022_stats.xlsx"):
        wb = excel.ExcelWorkbook(datetime.now())
        districts = defaultdict(list)
        for attendee in self.attendees:
            districts["All"].append(asdict(attendee).values())
            districts[attendee.district].append(asdict(attendee).values())
        for (title, rows) in list(districts.items()):
            wb.add_sheet(
                f"{title} Attendees",
                [
                    "Reg Num",
                    "Name",
                    "Name Badge",
                    "Club",
                    "District",
                    "Number of Pins",
                    "Number of Return Airport Transfers",
                    "Windbreakers",
                    "Dietary Requirements",
                    "Disability Requirements",
                ],
                widths={
                    "Reg Num": 20,
                    "Name": 40,
                    "Name Badge": 40,
                    "Club": 30,
                    "District": 15,
                    "Number of Return Airport Transfers": 40,
                    "Dietary Requirements": 50,
                    "Disability Requirements": 50,
                },
            )
            for row in rows:
                wb.add_row(list(row)[:-2])
        wb.add_sheet("Totals", ["Item", "Total"], widths={"Item": 30, "Total": 20})
        wb.add_row(["Total Attendees", self.total_attendees])
        wb.add_row(["Lions", self.lions])
        wb.add_row(["Non-Lions", self.non_lions])
        wb.add_row(["First MDC", self.first_mdc])
        wb.add_row(["MJF Lunch", self.mjf_lunch])
        wb.add_row(["PDG Dinner", self.pdg_dinner])
        wb.add_row(["President's Elect Breakfast", self.lpe_breakfast])
        wb.add_row(["Partner's Program", self.partner_program])
        wb.add_row(["Pins", self.pins])
        wb.add_row(["Return Airport Transfers", self.transport])
        for wbk in WINDBREAKER_KEYS:
            wb.add_row([f"Windbreakers: Size {wbk}", self.windbreakers[wbk]])
        wb.save(file_name)


if __name__ == "__main__":
    stats = Stats()
    stats.populate(collection)
    stats.make_excel()
    print(stats)
